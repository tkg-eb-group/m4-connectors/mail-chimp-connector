# Mailchimp Extension

Ksquare's Mailchimp Connector 1.0.1 for Mule 4 offers an out-of-the-box solution to integrate Mailchimp's marketing automation platform and email marketing service with other business applications.

Quickly and easily configure modules to access and control account and campaigns data in Mule flows.

In this first release Mailchimp Connector implements the following features for the MuleSoft-based enterprise solutions:

- Marketing API:
    - Automations API

More and more features will be covered release by release.

Application/Service         |	Version
--                          | --
Mule Runtime	            |  4.1
Mailchimp Marketing API     |   v. 3.0.23 Curl Client
Java	                    |  1.8 and later


## Highlights of Mailchimp Connector Mule 4

It allows users to query, create, modify and control automations and supports all the parameters for each operation of the Mailchimp Marketing API according to the Curl Client Documentation.
