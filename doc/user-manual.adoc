= Mailchimp Connector
:keywords: anypoint studio, connector, endpoint


https://www.mulesoft.com/legal/versioning-back-support-policy#anypoint-connectors[_Certified_]

== Overview
The Mailchimp connector was implemented using Java and consuming Mailchimp Marketing v. 3.0.23 API.

The connector exposes the following operations:

=== Automations

* List automations
* Add automation
* Get automation info
* Manage automation

==== Emails

* List automated emails
* Get workflow email info
* Delete workflow email
* Update workflow email
* Start/Pause automated email

==== Queue

* List automated email subscribers
* Add subscriber to workflow email
* Get automated email subscriber

==== Removed Subscribers

* List subscribers removed form workflow
* Remove subscriber from workflow
* Get subscriber removed from workflow

== Prerequisites

[%header%autowidth]
|===
| *Application/Service*         |	*Version*
| Mule Runtime	            |  4.X
| Mailchimp Marketing API     |   v. 3.0.23 Curl Client
| Java	                    |  1.8 and later
|===

This document assumes that you are familiar with Mule, Anypoint Connectors, Anypoint Studio, Mule concepts, elements in a Mule flow and Global Elements.

You need an API Key to test your connection to your target resource.

For hardware and software requirements and compatibility
information, see the Connector Release Notes.

To use this connector with Maven, view the pom.xml dependency information in
the Dependency Snippets in Anypoint Exchange.

== Configuration

An API Key needs to be provided for authentication. For more information about how to get an API Key visit: https://mailchimp.com/en/help/about-api-keys/

To find the value for the server parameter log into your Mailchimp account and look at the URL in your browser. You’ll see something like https://us19.admin.mailchimp.com/. The us19 part is the server prefix. Note that your specific value may be different.

image::./images/Mailchimp_Config.png[]

```xml
<mailchimp:config name="Mailchimp_Config" doc:name="Mailchimp Config" doc:id="5fdc553f-a003-4db5-9609-ae9557a16259" >
    <mailchimp:connection server="us2" apiKey="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-us2" />
</mailchimp:config>
```
 
== Operations

For all the operations most of the parameters are found in the General tab.

Body parameter supports the use of expressions or a static values. Use JSON bodies following the documentation: https://mailchimp.com/developer/api/marketing/

===== *Example:*

image::./images/Expressions.png[]

```xml
<mailchimp:add-subscriber-to-workflow-email doc:name="Automations: Queue - Add subscriber to workflow email" doc:id="472cea03-188e-4f23-b169-31b1efb958d1" config-ref="Mailchimp_Config" workflowId="1140a597a9" workflowEmailId="f66157b0db" emailAddressBody='#[{ "email":"email@domain.com" }]'>
</mailchimp:add-subscriber-to-workflow-email>

```

For List and Get operations there is also a Query Parameters tab. There you will allways find a Fields parameter.

Tick Specify Fields to enable it and add any number to the list to restict the fields to return in the response. Reference parameters of sub-objects with dot notation. You can also tick Exclude to return all but the specified fields.

image::./images/Fields1.png[]
image::./images/Fields2.png[]

=== *Automations*

==== List automations

Get a summary of an account's Automations.

===== *Query Parameters:*

- *Count:* The number of records to return. Default value is 10. Maximum value is 1000.
- *Offset:* The number of records from a collection to skip. Iterating over large collections with this parameter can be slow. Default value is 0.
- *Before create time:* Restrict the response to automations created before this time. A local date time with the following format: YYYY-MM-DDThh:mm:ss.
- *Since create time:* Restrict the response to automations created after this time. A local date time with the following format: YYYY-MM-DDThh:mm:ss.
- *Before send time:* Restrict the response to automations started before this time. A local date time with the following format: YYYY-MM-DDThh:mm:ss.
- *Since send time:* Restrict the response to automations started after this time. A local date time with the following format: YYYY-MM-DDThh:mm:ss.
- *Time Zone:* The Time Zone for Before created time, After created time, Before send time and After send time. An UTC with the following format: +00:00.
- *Status:* Restrict the results to automations with the specified status. One of: paused, send or sending.
- *Fields:* A list of fields to return. Reference parameters of sub-objects with dot notation.
- *Exclude:* Tick to return all but the specified fields.

===== *Example:*

image::./images/ListAutomations.png[]


```xml
<mailchimp:list-automations doc:name="Automations - List automations" doc:id="7c90bc43-2aba-46a1-9ce6-8ceec3acc506" config-ref="Mailchimp_Config" status="SENDING" count="20" offset="5" beforeCreateTime="2020-01-01T06:00:00" sinceCreateTime="2020-01-01T00:00:00" beforeSendTime="2020-01-01T12:00:00" sinceSendTime="2020-01-01T06:30:00">
    <mailchimp:field-values >
        <mailchimp:field-value field="automations.id" />
    </mailchimp:field-values>
</mailchimp:list-automations>
```

====  Add automation

Create a new Automation in your Mailchimp account.

===== *Required Parameters:*
Recipients

- *List ID:* The id of the List.
- *Store ID:* The id of the Store.

Trigger Settings

- *Workflow type:* The type of Automation workflow. One of: "ABANDONED_BROWSE", "ABANDONED_CART" or "EMAIL_FOLLOWUP".

===== *Optional Parameters:*

Settings

- *From name:* The "from" name for the Automation.
- *Reply to:* The reply-to email address for the Automation.

===== *Example:*

image::./images/AddAutomation.png[]

```xml
<mailchimp:add-automation doc:name="Automations - Add automation" doc:id="657ac871-5f1a-46cf-a053-b4be475b8647" config-ref="Mailchimp_Config">
    <mailchimp:add-automation-body >
        <mailchimp:add-automation-recipients listId="1646137f22" storeId="store_4vtmna8pwp8e6q3l5igb" />
        <mailchimp:add-automation-settings fromName="Rodrigo" replyTo="email@domain.com" />
        <mailchimp:trigger-settings workflowType="ABANDONED_CART" />
    </mailchimp:add-automation-body>
</mailchimp:add-automation>
```

==== Get automation info

Get a summary of an individual Automation workflow's settings and content.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 

===== *Query Parameters:*

- *Fields:* A list of fields to return. Reference parameters of sub-objects with dot notation.
- *Exclude:* Tick to return all but the specified fields.

===== *Example:*

image::./images/GetAutomationInfo.png[]

```xml
<mailchimp:get-automation-info doc:name="Automations - Get automation info" doc:id="7e3285d2-b099-4acd-85fc-0a778d0d921b" config-ref="Mailchimp_Config" workflowId="1140a597a9"/>
```

==== Manage automation

- Start all emails in an Automation workflow.
- Pause all emails in an Automation workflow.
- Archive an Automation.

===== *Required Parameters:*

- *Action*: The action to perform. One of: "START_ALL_EMAILS", "PAUSE_ALL_EMAILS" or "ARCHIVE".
- *Workflow ID:* The unique id for the Automation workflow. 

===== *Example:*

image::./images/ManageAutomation.png[]

```xml
<mailchimp:manage-automation doc:name="Automations - Manage automation" doc:id="0d05da56-a8d9-496c-ac60-1c5b62da6150" config-ref="Mailchimp_Config" manageAutomation="START_ALL_EMAILS" workflowId="1140a597a9"/>
```

=== Emails

==== List automated emails

Get a summary of the emails in an Automation workflow.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 

===== *Query Parameters:*

- *Count:* The number of records to return. Default value is 10. Maximum value is 1000.
- *Offset:* The number of records from a collection to skip. Iterating over large collections with this parameter can be slow. Default value is 0.
- *Fields:* A list of fields to return. Reference parameters of sub-objects with dot notation.
- *Exclude:* Tick to return all but the specified fields.

===== *Example:*

image::./images/ListAutomatedEmails.png[]

```xml
<mailchimp:list-automated-emails doc:name="Automations: Emails - List automated emails" doc:id="5c8fa48b-380f-4e6b-ad11-8c80ebde99b8" config-ref="Mailchimp_Config" workflowId="1140a597a9"/>

```

==== Get workflow email info

Get information about an individual Automation workflow email.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 
- *Workflow email ID*: The unique id for the Automation workflow email.

===== *Query Parameters:*

- *Fields:* A list of fields to return. Reference parameters of sub-objects with dot notation.
- *Exclude:* Tick to return all but the specified fields.

===== *Example:*

image::./images/GetWorkflowEmailInfo.png[]

```xml
<mailchimp:get-workflow-email-info doc:name="Automations: Emails - Get workflow email info" doc:id="90707569-f1de-456d-a646-4a84a301a2a7" config-ref="Mailchimp_Config" workflowId="1140a597a9" workflowEmailId="f66157b0db"/>
```

==== Delete workflow email

Removes an individual Automation workflow email.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 
- *Workflow email ID*: The unique id for the Automation workflow email.

===== *Example:*

image::./images/DeleteWorkflowEmail.png[]

```xml
<mailchimp:delete-workflow-email doc:name="Automations: Emails - Delete workflow email" doc:id="62fe4845-2ac9-4cda-8e99-b1623447c52c" config-ref="Mailchimp_Config" workflowEmailId="f66157b0db" workflowId="1140a597a9"/>
```

==== Update workflow email

Update settings for a Automation workflow email.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 
- *Workflow email ID*: The unique id for the Automation workflow email.

Delay

- *Type:* The delay amount for an automation email. One of: "NOW, "HOUR", "DAY" or "WEEK".
- *Direction:* The type of delay for an automation email. Currently only supports "AFTER".
- *Action:* The action that triggers the delay of an automation emails. One of: "SINGUP",
 "ECOMM_ABANDONED_BROWSE", "ECOMM_ABANDONED_CART".

===== *Optional Parameters:*

- *Subject line:* The subject line for the campaign.
- *Preview text:* The preview text for the campaign.
- *Title:* The title of the Automation.
- *From name:* The "from" name for the Automation (not an email address).
- *Reply to:* The reply-to email address for the Automation.

===== *Example:*

image::./images/UpdateWorkflowEmail.png[]

```xml
<mailchimp:update-workflow-email doc:name="Automations: Emails - Update workflow email" doc:id="0bbcbf56-0a67-4a7c-934b-bbe617270711" config-ref="Mailchimp_Config" workflowId="1140a597a9" workflowEmailId="f66157b0db">
    <mailchimp:update-email-body >
        <mailchimp:delay amount="1" type="DAY" direction="AFTER" action="SIGNUP" />
        <mailchimp:update-email-settings subjectLine="subject" previewText="preview" title="title" fromName="Rodrigo" replyTo="email@domain.com" />
    </mailchimp:update-email-body>
</mailchimp:update-workflow-email>
```

==== Start/Pause automated email

Start or Pause an automated email.

===== *Required Parameters:*

 - *Action*: The action to perform. One of: "START" or "PAUSE".
- *Workflow ID:* The unique id for the Automation workflow. 
- *Workflow email ID*: The unique id for the Automation workflow email.

===== *Example:*

image::./images/StartPauseAutomatedEmail.png[]

```xml
<mailchimp:start-pause-automated-email doc:name="Automations: Emails - Start/Pause automated email" doc:id="71721ff6-3453-4473-9f47-2c75c7750788" config-ref="Mailchimp_Config" startPause="START" workflowId="1140a597a9" workflowEmailId="f66157b0db"/>

```

==== Queue

==== List automated email subscribers

Get information about an Automation email queue.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 
- *Workflow email ID*: The unique id for the Automation workflow email.

===== *Query Parameters:*

- *Count:* The number of records to return. Default value is 10. Maximum value is 1000.
- *Offset:* The number of records from a collection to skip. Iterating over large collections with this parameter can be slow. Default value is 0.
- *Fields:* A list of fields to return. Reference parameters of sub-objects with dot notation.
- *Exclude:* Tick to return all but the specified fields.

===== *Example:*

image::./images/ListAutomatedEmailSubscribers.png[]

```xml
<mailchimp:list-automated-email-subscribers doc:name="Automations: Queue - List automated emails subscribers" doc:id="da02e5c1-a50e-4ffd-b92b-b282eb94e422" config-ref="Mailchimp_Config" workflowId="1140a597a9" workflowEmailId="f66157b0db"/>
```

==== Add subscriber to workflow email

Manually add a subscriber to a workflow, bypassing the default trigger settings.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 
- *Workflow email ID*: The unique id for the Automation workflow email.
- *Email address:* The list member's email address.

===== *Example:*

image::./images/AddSubscribersToWorkflowEmail.png[]

```xml
<mailchimp:add-subscriber-to-workflow-email doc:name="Automations: Queue - Add subscriber to workflow email" doc:id="f920c05d-3ccf-4556-8d81-8941d4c3838d" config-ref="Mailchimp_Config" workflowId="1140a597a9" workflowEmailId="f66157b0db">
    <mailchimp:email-address-body email="email@domain.com" />
</mailchimp:add-subscriber-to-workflow-email>
```

==== Get automated email subscriber

Get information about a specific subscriber in an Automation email queue.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 
- *Workflow email ID*: The unique id for the Automation workflow email.
- Subscriber hash:

===== *Query Parameters:*

- *Fields:* A list of fields to return. Reference parameters of sub-objects with dot notation.
- *Exclude:* Tick to return all but the specified fields.

===== *Example:*

image::./images/GetAutomatedEmailSubscriber.png[]

```xml
<mailchimp:get-automated-email-subscriber doc:name="Automations: Queue - Get automated email subscriber" doc:id="189cbfd5-f1bf-4f93-94fb-828199eb9405" config-ref="Mailchimp_Config" workflowId="1140a597a9" workflowEmailId="f66157b0db" subscriberHash="ce250daa748d790266a113a2fb06fd3c"/>
```

==== Removed Subscribers

==== List subscribers removed form workflow

Get information about subscribers who were removed from an Automation workflow.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 

===== *Query Parameters:*

- *Count:* The number of records to return. Default value is 10. Maximum value is 1000.
- *Offset:* The number of records from a collection to skip. Iterating over large collections with this parameter can be slow. Default value is 0.
- *Fields:* A list of fields to return. Reference parameters of sub-objects with dot notation.
- *Exclude:* Tick to return all but the specified fields.

===== *Example:*

image::./images/ListSubscribersRemovedFormWorkflow.png[]

```xml
<mailchimp:list-subscribers-removed-from-workflow doc:name="Automations: Removed Subscribers - List subscribers removed from workflow" doc:id="27cae6ad-0fef-4c77-a75b-981e7d06a089" config-ref="Mailchimp_Config" workflowId="1140a597a9"/>
```

==== Remove subscriber from workflow

Remove a subscriber from a specific Automation workflow.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 
- *Email address:* The list member's email address.

===== *Example:*

image::./images/RemoveSubscriberFromWorkflow.png[]

```xml
<mailchimp:remove-subscriber-from-workflow doc:name="Automations: Removed Subscribers - Remove subscriber from workflow" doc:id="c705b8d2-9eac-46d1-bf3e-2fe88f411d74" config-ref="Mailchimp_Config" workflowId="1140a597a9">
    <mailchimp:email-address-body email="email@domain.com" />
</mailchimp:remove-subscriber-from-workflow>
```

==== Get subscriber removed from workflow

Get information about a specific subscriber who was removed from an Automation workflow.

===== *Required Parameters:*

- *Workflow ID:* The unique id for the Automation workflow. 
- *Subscriber hash:* he MD5 hash of the lowercase version of the list member's email address.

===== *Query Parameters:*

- *Fields:* A list of fields to return. Reference parameters of sub-objects with dot notation.
- *Exclude:* Tick to return all but the specified fields.

===== *Example:*

image::./images/GetSubscriberRemovedFromWorkflow.png[]

```xml
<mailchimp:get-subscriber-removed-from-workflow doc:name="Automations: Removed Subscribers - Get subscriber removed from workflow" doc:id="23050c4c-d805-4c99-ad5d-a001a2b281d1" config-ref="Mailchimp_Config" workflowId="1140a597a9" subscriberHash="ce250daa748d790266a113a2fb06fd3c"/>
```

For more information about Automations operations review https://mailchimp.com/developer/api/marketing/automation/

