/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public enum ManageAutomation implements Serializable {
    @JsonProperty("start_all_emails")
    START_ALL_EMAILS,
    @JsonProperty("pause_all_emails")
    PAUSE_ALL_EMAILS,
    @JsonProperty("archive")
    ARCHIVE
}
