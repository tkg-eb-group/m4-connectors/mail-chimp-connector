/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.automations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

import java.io.Serializable;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class AddAutomationBody  implements Serializable {

    @Parameter
    @DisplayName("Recipients")
    @Alias("addAutomationRecipients")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private AutomationRecipients recipients;

    @Parameter
    @DisplayName("Settings")
    @Optional
    @Alias("addAutomationSettings")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private AutomationSettings settings;

    @Parameter
    @DisplayName("Trigger Settings")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @JsonProperty("trigger_settings")
    private TriggerSettings triggerSettings;

    public AutomationRecipients getRecipients() {
        return recipients;
    }

    public void setRecipients(AutomationRecipients recipients) {
        this.recipients = recipients;
    }

    public AutomationSettings getSettings() {
        return settings;
    }

    public void setSettings(AutomationSettings settings) {
        this.settings = settings;
    }

    public TriggerSettings getTriggerSettings() {
        return triggerSettings;
    }

    public void setTriggerSettings(TriggerSettings triggerSettings) {
        this.triggerSettings = triggerSettings;
    }
}
