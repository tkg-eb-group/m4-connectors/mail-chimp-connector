/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.automations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class AutomationRecipients implements Serializable {

    @Parameter
    @DisplayName("List ID")
    @Summary("The id of the list.")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @JsonProperty("list_id")
    private String listId;

    @Parameter
    @DisplayName("Store ID")
    @Summary("The id of the store.")
    @JsonProperty("store_id")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private String storeId;

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
}
