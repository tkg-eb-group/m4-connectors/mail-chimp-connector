/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.automations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class AutomationSettings implements Serializable {

    @Parameter
    @DisplayName("From name")
    @Summary("The 'from' name for the Automation (not an email address).")
    @Optional
    @Example("John Smith")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @JsonProperty("from_name")
    private String fromName;

    @Parameter
    @DisplayName("Reply to")
    @Summary("The reply-to email address for the Automation.")
    @Example("email@domain.com")
    @Optional
    @JsonProperty("reply_to")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private String replyTo;

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
}
