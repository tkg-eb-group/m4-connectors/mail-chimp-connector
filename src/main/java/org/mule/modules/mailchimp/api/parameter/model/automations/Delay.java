/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.automations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mule.modules.mailchimp.api.domain.Action;
import org.mule.modules.mailchimp.api.domain.DelayDirection;
import org.mule.modules.mailchimp.api.domain.DelayType;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class Delay implements Serializable {

    @Parameter
    @DisplayName("Amount")
    @Summary("The delay amount for an automation email.")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private int amount;

    @Parameter
    @Alias("type")
    @Summary("The type of delay for an automation email.")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private DelayType type;

    @Parameter
    @DisplayName("Direction")
    @Summary("Whether the delay settings describe before or after the delay action of an automation email.")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private DelayDirection direction;

    @Parameter
    @DisplayName("Action")
    @Summary("The action that triggers the delay of an automation emails.")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private Action action;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public DelayType getType() {
        return type;
    }

    public void setType(DelayType type) {
        this.type = type;
    }

    public DelayDirection getDirection() {
        return direction;
    }

    public void setDirection(DelayDirection direction) {
        this.direction = direction;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
