/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.automations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class EmailAddress implements Serializable {

    @Parameter
    @DisplayName("Email address")
    @Summary("The list member's email address.")
    @Example("email@domain.com")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @JsonProperty("email_address")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
