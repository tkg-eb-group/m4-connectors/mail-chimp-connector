/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.automations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.modules.mailchimp.api.domain.WorkflowType;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class TriggerSettings implements Serializable {

    @Parameter
    @DisplayName("Workflow type")
    @Summary("The type of Automation workflow.")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @JsonProperty("workflow_type")

    private WorkflowType workflowType;
    public WorkflowType getWorkflowType() {
        return workflowType;
    }
    public void setWorkflowType(WorkflowType workflowType) {
        this.workflowType = workflowType;
    }
}
