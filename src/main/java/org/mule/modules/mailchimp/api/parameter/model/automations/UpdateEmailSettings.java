/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.automations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class UpdateEmailSettings extends UpdateAutomationSettings{

    @Parameter
    @DisplayName("Subject line")
    @Summary("The subject line for the campaign.")
    @Optional
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @JsonProperty("subject_line")
    private String subjectLine;

    @Parameter
    @DisplayName("Preview text")
    @Summary("The preview text for the campaign.")
    @Optional
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @JsonProperty("preview_text")
    private String previewText;

    public String getSubjectLine() {
        return subjectLine;
    }

    public void setSubjectLine(String subjectLine) {
        this.subjectLine = subjectLine;
    }

    public String getPreviewText() {
        return previewText;
    }

    public void setPreviewText(String previewText) {
        this.previewText = previewText;
    }
}
