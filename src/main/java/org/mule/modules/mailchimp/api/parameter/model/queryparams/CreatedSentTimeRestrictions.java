/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.queryparams;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.api.util.MultiMap;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;
import java.time.LocalDateTime;

public class CreatedSentTimeRestrictions extends TimeQueryParameters implements Serializable {

    private static final String QUERY_PARAMETERS_TAB = "Query Parameters";

    @Parameter
    @DisplayName("Before create time")
    @Summary("Restrict the response to items created before the set time.")
    @Optional
    @Example("YYYY-MM-DDThh:mm:ss")
    @Placement(tab = QUERY_PARAMETERS_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    LocalDateTime beforeCreateTime;

    @Parameter
    @DisplayName("Since create time")
    @Summary("Restrict the response to items created after the set time.")
    @Optional
    @Example("YYYY-MM-DDThh:mm:ss")
    @Placement(tab = QUERY_PARAMETERS_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    LocalDateTime sinceCreateTime;

    @Parameter
    @DisplayName("Before send time")
    @Summary("Restrict the response to items sent before the set time.")
    @Optional
    @Example("YYYY-MM-DDThh:mm:ss")
    @Expression(ExpressionSupport.SUPPORTED)
    @Placement(tab = QUERY_PARAMETERS_TAB)
    LocalDateTime beforeSendTime;

    @Parameter
    @DisplayName("Since send time")
    @Summary("Restrict the response to items sent since the set time.")
    @Optional
    @Example("YYYY-MM-DDThh:mm:ss")
    @Expression(ExpressionSupport.SUPPORTED)
    @Placement(tab = QUERY_PARAMETERS_TAB)
    LocalDateTime sinceSendTime;

    public LocalDateTime getBeforeCreateTime() {
        return beforeCreateTime;
    }


    public LocalDateTime getSinceCreateTime() {
        return sinceCreateTime;
    }


    public LocalDateTime getBeforeSendTime() {
        return beforeSendTime;
    }


    public LocalDateTime getSinceSendTime() {
        return sinceSendTime;
    }


    public void specify(MultiMap<String, String> queryParams){

        String beforeCreateTimeStr;
        String sinceCreateTimeStr;
        String beforeSendTimeStr;
        String sinceSendTimeStr;

        if (getBeforeCreateTime() != null){
            beforeCreateTimeStr = getBeforeCreateTime().toString() + getTimeZoneStr();
            queryParams.put("before_create_time", beforeCreateTimeStr);
        }

        if ( getSinceCreateTime() != null){
            sinceCreateTimeStr =  getSinceCreateTime().toString() + getTimeZoneStr();
            queryParams.put("since_create_time", sinceCreateTimeStr);
        }

        if (getBeforeSendTime() != null){
            beforeSendTimeStr = getBeforeSendTime().toString() + getTimeZoneStr();
            queryParams.put("before_send_time", beforeSendTimeStr);
        }

        if (getSinceSendTime() != null){
            sinceSendTimeStr = getSinceSendTime().toString() + getTimeZoneStr();
            queryParams.put("since_send_time", sinceSendTimeStr);
        }
    }
}
