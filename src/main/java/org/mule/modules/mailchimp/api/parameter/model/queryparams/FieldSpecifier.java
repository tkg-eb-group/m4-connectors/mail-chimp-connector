/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.queryparams;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.api.util.MultiMap;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

public class FieldSpecifier implements Serializable {

    private static final String QUERY_PARAMETERS_TAB = "Query Parameters";

    @Parameter
    @DisplayName("Specify Fields")
    @Optional
    @NullSafe
    @Placement(tab = QUERY_PARAMETERS_TAB)
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private Set<FieldValue> fieldValues;

    @Parameter
    @DisplayName("Exclude")
    @Placement(tab = QUERY_PARAMETERS_TAB)
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private boolean exclude;

    public Set<FieldValue> getFieldValues() {
        return fieldValues;
    }

    public boolean isExclude() {
        return exclude;
    }


    public String getStrFields(){
        return getFieldValues()
                .stream()
                .map(FieldValue::getField)
                .collect(Collectors.joining(","));
    }

    public void specify(MultiMap<String, String> queryParams){
        if (getFieldValues() != null){
            if (isExclude()) {
                queryParams.put("exclude_fields", getStrFields());
            } else {
                queryParams.put("fields", getStrFields());
            }
        }
    }

}

