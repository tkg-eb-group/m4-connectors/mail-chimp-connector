/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.queryparams;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

public class FieldValue implements Serializable {

	@Parameter
	@Summary("Reference parameters of sub-objects with dot notation")
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	private String field;

	public String getField() { return this.field; }
}
