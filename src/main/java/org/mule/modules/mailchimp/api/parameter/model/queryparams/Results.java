/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.queryparams;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.api.util.MultiMap;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class Results implements Serializable {

    private static final String QUERY_PARAMETERS_TAB = "Query Parameters";

    @Parameter
    @DisplayName("Count")
    @Summary("The number of records to return. Maximum value is 1000")
    @Optional (defaultValue = "10")
    @Placement(tab = QUERY_PARAMETERS_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    private int count;

    @Parameter
    @DisplayName("Offset")
    @Summary("The number of records from a collection to skip.")
    @Optional(defaultValue = "0")
    @Placement(tab = QUERY_PARAMETERS_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    private int offset;

    public int getCount() {
        return count;
    }

    public int getOffset() {
        return offset;
    }

    public void specify(MultiMap<String, String> queryParams){
        queryParams.put("count", String.valueOf(getCount()));
        queryParams.put("offset", String.valueOf(getOffset()));
    }
}
