/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.api.parameter.model.queryparams;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;

import java.io.Serializable;

public class TimeQueryParameters implements Serializable {

    private static final String QUERY_PARAMETERS_TAB = "Query Parameters";

    @Parameter
    @DisplayName("Time zone (UTC)")
    @Optional(defaultValue = "+00:00")
    @Placement(tab = QUERY_PARAMETERS_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    String timeZoneStr;

    public String getTimeZoneStr() {
        return timeZoneStr;
    }

}
