/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.configuration;

import org.mule.modules.mailchimp.internal.connection.provider.MailchimpConnectionProvider;
import org.mule.modules.mailchimp.internal.operation.AutomationsOperations;
import org.mule.runtime.extension.api.annotation.Configuration;
import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.connectivity.ConnectionProviders;

@Configuration(name = "config")
@Operations({
        AutomationsOperations.class
})
@ConnectionProviders(MailchimpConnectionProvider.class)
public class MailchimpConfiguration {

}
