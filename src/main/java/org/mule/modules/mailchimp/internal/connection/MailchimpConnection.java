/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.connection;

import org.mule.modules.mailchimp.internal.configuration.MailchimpConnectionConfiguration;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.http.api.HttpConstants.Method;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * This class represents an extension connection just as example (there is no
 * real connection with anything here c:).
 */
public final class MailchimpConnection {

	private static final Logger logger = LoggerFactory.getLogger(MailchimpConnection.class);

	private HttpClient httpClient;
	private String strUri;
	private String strCredentials;
	private int timeout;
	private TimeUnit timeoutUnit;

	public MailchimpConnection(HttpClient httpClient, MailchimpConnectionConfiguration config) {

		this.strUri = "https://" + config.getServer() + ".api.mailchimp.com/3.0";
		this.strCredentials =  Base64.getEncoder().encodeToString(("user:"+config.getApiKey()).getBytes(StandardCharsets.UTF_8));
		this.timeout = config.getTimeout();
		this.timeoutUnit = config.getTimeoutUnit();
		this.httpClient = httpClient;
	}

	public boolean isConnected() throws ConnectionException, IOException, TimeoutException {
		HttpRequest request = HttpRequest.builder()
				.method(Method.GET)
				.uri(strUri + "/ping")
				.addHeader("Authorization", "Basic " + strCredentials)
				.build();

		HttpResponse httpResponse = httpClient.send(request, getTimeoutAsMilliseconds(), false, null);
		
		if (httpResponse.getStatusCode() >= 200 && httpResponse.getStatusCode() < 300) {
			return true;
		} else {
			ConnectionException e =  new ConnectionException(
					"Error connecting to the server: Error Code " + httpResponse.getStatusCode() + "~" + httpResponse);
			logger.error("Connection error occurred in MailchimpConnection::isConnected", e);
			throw e;
		}
	}

	public int getTimeoutAsMilliseconds(){
		return (int)TimeUnit.MILLISECONDS.convert(getTimeout(), getTimeoutUnit());
	}

	public String getStrUri() {
		return strUri;
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public HttpClient getHttpClient() {
		return httpClient;
	}

	public String getStrCredentials() {
		return strCredentials;
	}

	public int getTimeout() {
		return timeout;
	}

	public TimeUnit getTimeoutUnit() {
		return timeoutUnit;
	}
}
