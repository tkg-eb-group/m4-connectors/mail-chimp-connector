/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.connection.provider;

import org.mule.modules.mailchimp.internal.configuration.MailchimpConnectionConfiguration;
import org.mule.modules.mailchimp.internal.error.MailchimpErrorType;
import org.mule.modules.mailchimp.internal.error.exception.MailchimpException;
import org.mule.runtime.api.connection.*;
import org.mule.runtime.api.lifecycle.Startable;
import org.mule.runtime.api.lifecycle.Stoppable;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;

import javax.inject.Inject;

import org.mule.modules.mailchimp.internal.connection.MailchimpConnection;
import org.mule.runtime.extension.api.annotation.param.RefName;
import org.mule.runtime.extension.api.error.MuleErrors;
import org.mule.runtime.http.api.HttpService;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.client.HttpClientConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class (as it's name implies) provides connection instances and the
 * functionality to disconnect and validate those connections.
 * <p>
 * All connection related parameters (values required in order to create a
 * connection) must be declared in the connection providers.
 * <p>
 * This particular example is a {@link PoolingConnectionProvider} which declares
 * that connections resolved by this provider will be pooled and reused. There
 * are other implementations like {@link CachedConnectionProvider} which lazily
 * creates and caches connections or simply {@link ConnectionProvider} if you
 * want a new connection each time something requires one.
 */
public class MailchimpConnectionProvider implements CachedConnectionProvider<MailchimpConnection>, Startable, Stoppable {

	private static final Logger logger = LoggerFactory.getLogger(MailchimpConnectionProvider.class);

	@Inject
	private HttpService httpService;

	private HttpClient httpClient;

	@RefName
	private String configName;

	@ParameterGroup(name = "Connection")
	MailchimpConnectionConfiguration connectionConfiguration;

	@Override
	public void start() {
		HttpClientConfiguration httpClientConfiguration = new HttpClientConfiguration.Builder()
				.setName(configName)
				.setProxyConfig(connectionConfiguration.getProxyConfig())
				.build();
		httpClient = httpService.getClientFactory().create(httpClientConfiguration);
		httpClient.start();
	}

	@Override
	public void stop() {
		if (httpClient != null) {
			httpClient.stop();
		}
	}

	@Override
	public MailchimpConnection connect() {
		return new MailchimpConnection(httpClient, connectionConfiguration);
	}

	@Override
	public void disconnect(MailchimpConnection connection) {
		connection.setHttpClient(null);
	}

	@Override
	public ConnectionValidationResult validate(MailchimpConnection connection) {
		ConnectionValidationResult result;
		try {
			connection.isConnected();
			result = ConnectionValidationResult.success();
		} catch (Exception e) {
			logger.error("Connection error occurred in MailchimpConnectionProvider::validate", e);
			result = ConnectionValidationResult.failure("Connection Failed: " + e.getMessage(),
			 	new MailchimpException("Error calling HTTP client", MuleErrors.CONNECTIVITY));
		}
		return result;
	}
}
