/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.error;

import org.mule.runtime.extension.api.error.ErrorTypeDefinition;
import org.mule.runtime.extension.api.error.MuleErrors;

import static java.util.Optional.ofNullable;
import java.util.Optional;

public enum MailchimpErrorType implements ErrorTypeDefinition<MailchimpErrorType> {

    PARSING(MuleErrors.TRANSFORMATION),
    HTTP_RESPONSE;

    private ErrorTypeDefinition<?> parent;

    MailchimpErrorType() {

    }

    MailchimpErrorType(final ErrorTypeDefinition<?> parent) {
        this.parent = parent;
    }

    @Override
    public Optional<ErrorTypeDefinition<? extends Enum<?>>> getParent() {
        return ofNullable(parent);
    }
}
