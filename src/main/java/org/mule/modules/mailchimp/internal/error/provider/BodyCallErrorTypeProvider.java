/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.error.provider;

import org.mule.modules.mailchimp.internal.error.MailchimpErrorType;
import org.mule.runtime.extension.api.error.ErrorTypeDefinition;

import java.util.Set;

public class BodyCallErrorTypeProvider extends CallErrorTypeProvider {

    @Override
    public Set<ErrorTypeDefinition<MailchimpErrorType>> getMailchimpErrorTypes() {
        Set<ErrorTypeDefinition<MailchimpErrorType>> errors = super.getMailchimpErrorTypes();
        errors.add(MailchimpErrorType.PARSING);
        return errors;
    }
}
