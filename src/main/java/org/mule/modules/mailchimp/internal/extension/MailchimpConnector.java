/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.extension;

import org.mule.modules.mailchimp.api.parameter.model.connection.DefaultProxyConfig;
import org.mule.modules.mailchimp.internal.configuration.MailchimpConfiguration;
import org.mule.modules.mailchimp.internal.error.MailchimpErrorType;
import org.mule.runtime.extension.api.annotation.*;
import org.mule.runtime.api.meta.Category;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;
import org.mule.runtime.extension.api.annotation.error.ErrorTypes;
import org.mule.runtime.extension.api.annotation.license.RequiresEnterpriseLicense;
import org.mule.runtime.http.api.client.proxy.ProxyConfig;

/**
 * This is the main class of an extension, is the entry point from which configurations, connection providers, operations
 * and sources are going to be declared.
 */
@Xml(prefix = "mailchimp")
@Extension(name = "Mailchimp", vendor = "com.ksquaregroup", category = Category.CERTIFIED)
@RequiresEnterpriseLicense(allowEvaluationLicense = true)
@Configurations(MailchimpConfiguration.class)
@ErrorTypes(MailchimpErrorType.class)
@SubTypeMapping(baseType = ProxyConfig.class, subTypes = {DefaultProxyConfig.class})
public class MailchimpConnector {

}
