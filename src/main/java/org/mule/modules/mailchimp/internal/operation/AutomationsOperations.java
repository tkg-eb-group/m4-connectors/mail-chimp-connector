/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.operation;

import org.mule.modules.mailchimp.api.domain.AutomationStatus;
import org.mule.modules.mailchimp.api.domain.ManageAutomation;
import org.mule.modules.mailchimp.api.domain.StartPause;
import org.mule.modules.mailchimp.api.parameter.model.automations.AddAutomationBody;
import org.mule.modules.mailchimp.api.parameter.model.automations.EmailAddress;
import org.mule.modules.mailchimp.api.parameter.model.automations.UpdateEmailBody;
import org.mule.modules.mailchimp.api.parameter.model.queryparams.CreatedSentTimeRestrictions;
import org.mule.modules.mailchimp.api.parameter.model.queryparams.FieldSpecifier;
import org.mule.modules.mailchimp.api.parameter.model.queryparams.Results;
import org.mule.modules.mailchimp.internal.connection.MailchimpConnection;
import org.mule.modules.mailchimp.internal.error.provider.BodyCallErrorTypeProvider;
import org.mule.modules.mailchimp.internal.error.provider.CallErrorTypeProvider;
import org.mule.modules.mailchimp.internal.service.HttpCallService;
import org.mule.modules.mailchimp.internal.util.ExpressionResolver;
import org.mule.modules.mailchimp.internal.util.TimeZoneValidator;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.api.util.MultiMap;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;
import org.mule.runtime.http.api.HttpConstants.Method;

import java.io.InputStream;


/**
 * This class is a container for operations, every public method in this class
 * will be taken as an extension operation.
 */
public class AutomationsOperations {

	private static final String QUERY_PARAMETERS_TAB = "Query Parameters";

	private static final String ENDPOINT = "/automations";

	private static final String START_ALL = "/actions/start-all-emails";
	private static final String PAUSE_ALL = "/actions/pause-all-emails";
	private static final String ARCHIVE = "/actions/archive";
	private static final String EMAILS = "/emails";
	private static final String PAUSE = "/actions/pause";
	private static final String START = "/actions/start";
	private static final String QUEUE = "/queue";
	private static final String REMOVED_SUBS = "/removed-subscribers";
	
	private final TimeZoneValidator timeZoneValidator = new TimeZoneValidator();

	@OutputJsonType(schema = "metadata/automations/list-automations-schema.json")
	@DisplayName("Automations - List automations")
	@Summary("Get a summary of an account's Automations.")
	@MediaType(value = "application/json")
	@Throws(BodyCallErrorTypeProvider.class)
	public InputStream listAutomations(
		@Connection MailchimpConnection connection,

		@ParameterGroup(name = "Results")
	    @Expression(ExpressionSupport.NOT_SUPPORTED)
	    @ParameterDsl(allowReferences = false)
	    Results results,

		@ParameterGroup(name = "Dates")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		CreatedSentTimeRestrictions createdSentTimeRestrictions,

		@DisplayName("Status")
		@Summary("Restrict the results to automations with the specified status.")
		@Optional
		@Placement(tab = QUERY_PARAMETERS_TAB)
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		AutomationStatus status,

		@ParameterGroup(name = "Fields")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		FieldSpecifier fieldSpecifier
	) {

		String strUri = connection.getStrUri() + ENDPOINT;
		MultiMap<String, String> queryParams = new MultiMap<>();

		results.specify(queryParams);
		timeZoneValidator.validate(createdSentTimeRestrictions.getTimeZoneStr());
		createdSentTimeRestrictions.specify(queryParams);
		fieldSpecifier.specify(queryParams);

		if (status != null) {
			queryParams.put("status", status.name().toLowerCase());
		}

		return HttpCallService.call(connection, Method.GET, strUri, queryParams);
	}

	@OutputJsonType(schema = "metadata/automations/automation-schema.json")
	@DisplayName("Automations - Add automation")
	@Summary("Create a new Automation in your Mailchimp account.")
	@MediaType(value = "application/json")
	@Throws(BodyCallErrorTypeProvider.class)
	public InputStream addAutomation(
		@Connection MailchimpConnection connection,

		@DisplayName("Body")
		@Expression(ExpressionSupport.SUPPORTED)
		@ParameterDsl(allowReferences = false)
		ParameterResolver<AddAutomationBody> addAutomationBody

	) {
		ExpressionResolver<AddAutomationBody> resolver = new ExpressionResolver<>();
		AddAutomationBody addAutomation = resolver.resolve(addAutomationBody, AddAutomationBody.class);

		String strUri = connection.getStrUri() + ENDPOINT;
		return HttpCallService.call(connection, Method.POST, strUri, addAutomation);
	}

	@OutputJsonType(schema = "metadata/automations/automation-schema.json")
	@DisplayName("Automations - Get automation info")
	@Summary("Get a summary of an individual Automation workflow's settings and content. The trigger_settings object " +
			"returns information for the first email in the workflow.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream getAutomationInfo(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@ParameterGroup(name = "Fields")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		FieldSpecifier fieldSpecifier

	) {
		String strUri = connection.getStrUri() + ENDPOINT +  "/" + workflowId;
		MultiMap<String, String> queryParams = new MultiMap<>();
		fieldSpecifier.specify(queryParams);

		return HttpCallService.call(connection, Method.GET, strUri, queryParams);
	}

	@DisplayName("Automations - Manage automation")
	@Summary(
			"Start all emails in an Automation workflow."
			+ "\n\n" +
			"Pause all emails in an Automation workflow."
			+ "\n\n" +
			"Archive an Automation, permanently end it automation and keep the report " +
			"data. You'll be able to replicate your archived automation, but you can't restart it."
	)
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream manageAutomation(
		@Connection MailchimpConnection connection,

		@DisplayName("Action")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		ManageAutomation manageAutomation,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId

	) {

		String action;
		switch (manageAutomation){
			case START_ALL_EMAILS:
				action = START_ALL;
				break;
			case ARCHIVE:
				action = ARCHIVE;
				break;
			case PAUSE_ALL_EMAILS:
			default:
				action = PAUSE_ALL;
				break;
		}

		String strUri = connection.getStrUri() + ENDPOINT +  "/" + workflowId + action;
		return HttpCallService.call(connection, Method.POST, strUri);
	}

	/*
	 * Emails
	 */

	@OutputJsonType(schema = "metadata/automations/list-automated-emails-schema.json")
	@DisplayName("Automations: Emails - List automated emails")
	@Summary("Get a summary of the emails in an Automation workflow.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream listAutomatedEmails(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@ParameterGroup(name = "Results")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		Results results,

		@ParameterGroup(name = "Fields")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		FieldSpecifier fieldSpecifier

	) {
		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + EMAILS;
		MultiMap<String, String> queryParams = new MultiMap<>();

		results.specify(queryParams);
		fieldSpecifier.specify(queryParams);

		return HttpCallService.call(connection, Method.GET, strUri, queryParams);
	}

	@OutputJsonType(schema = "metadata/automations/workflow-email-info-schema.json")
	@DisplayName("Automations: Emails - Get workflow email info")
	@Summary("Get information about an individual Automation workflow email.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream getWorkflowEmailInfo(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Workflow email ID")
		@Summary("The unique id for the Automation workflow email.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowEmailId,

		@ParameterGroup(name = "Fields")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		FieldSpecifier fieldSpecifier

	) {
		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + EMAILS + "/" + workflowEmailId;
		MultiMap<String, String> queryParams = new MultiMap<>();

		fieldSpecifier.specify(queryParams);

		return HttpCallService.call(connection, Method.GET, strUri, queryParams);
	}

	@DisplayName("Automations: Emails - Delete workflow email")
	@Summary("Removes an individual Automation workflow email. Emails from certain workflow types, including the " +
			"Abandoned Cart Email (abandonedCart) and Product Retargeting Email (abandonedBrowse) Workflows, cannot " +
			"be deleted.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream deleteWorkflowEmail(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Workflow email ID")
		@Summary("The unique id for the Automation workflow email.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowEmailId

	) {
		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + EMAILS + "/" + workflowEmailId;
		return HttpCallService.call(connection, Method.DELETE, strUri);
	}

	@OutputJsonType(schema = "metadata/automations/workflow-email-info-schema.json")
	@DisplayName("Automations: Emails - Update workflow email")
	@Summary("Update settings for a Automation workflow email.")
	@MediaType(value = "application/json")
	@Throws(BodyCallErrorTypeProvider.class)
	public InputStream updateWorkflowEmail(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Workflow email ID")
		@Summary("The unique id for the Automation workflow email.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowEmailId,

		@DisplayName("Body")
		@Expression(ExpressionSupport.SUPPORTED)
		@ParameterDsl(allowReferences = false)
		ParameterResolver<UpdateEmailBody> updateEmailBody

	) {
		ExpressionResolver<UpdateEmailBody> resolver = new ExpressionResolver<>();
		UpdateEmailBody updateEmail = resolver.resolve(updateEmailBody, UpdateEmailBody.class);

		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + EMAILS + "/" + workflowEmailId;
		return HttpCallService.call(connection, Method.PATCH, strUri, updateEmail);
	}

	@DisplayName("Automations: Emails - Start/Pause automated email")
	@Summary("Start/Pause an automated email.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream startPauseAutomatedEmail(
		@Connection MailchimpConnection connection,

		@DisplayName("Action")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		StartPause startPause,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Workflow email ID")
		@Summary("The unique id for the Automation workflow email.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowEmailId

	) {
		String action;
		if (startPause == StartPause.START) {
			action = START;
		} else {
			action = PAUSE;
		}

		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + EMAILS + "/" + workflowEmailId + action;
		return HttpCallService.call(connection, Method.POST, strUri);
	}

	/*
	 * Queue
	 */

	@OutputJsonType(schema = "metadata/automations/list-automated-email-subscribers-schema.json")
	@DisplayName("Automations: Queue - List automated emails subscribers")
	@Summary("Get information about an Automation email queue.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream listAutomatedEmailSubscribers(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Workflow email ID")
		@Summary("The unique id for the Automation workflow email.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowEmailId,

		@ParameterGroup(name = "Results")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		Results results,

		@ParameterGroup(name = "Fields")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		FieldSpecifier fieldSpecifier

	) {
		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + EMAILS + "/" + workflowEmailId + QUEUE;
		MultiMap<String, String> queryParams = new MultiMap<>();

		results.specify(queryParams);
		fieldSpecifier.specify(queryParams);

		return HttpCallService.call(connection, Method.GET, strUri, queryParams);
	}

	@OutputJsonType(schema = "metadata/automations/automated-email-subscriber-schema.json")
	@DisplayName("Automations: Queue - Add subscriber to workflow email")
	@Summary("Manually add a subscriber to a workflow, bypassing the default trigger settings. You can also use this " +
			"endpoint to trigger a series of automated emails in an API 3.0 workflow type.")
	@MediaType(value = "application/json")
	@Throws(BodyCallErrorTypeProvider.class)
	public InputStream addSubscriberToWorkflowEmail(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Workflow email ID")
		@Summary("The unique id for the Automation workflow email.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowEmailId,

		@DisplayName("Body")
		@Expression(ExpressionSupport.SUPPORTED)
		@ParameterDsl(allowReferences = false)
		ParameterResolver<EmailAddress> emailAddressBody

	) {
		ExpressionResolver<EmailAddress> resolver = new ExpressionResolver<>();
		EmailAddress emailAddress = resolver.resolve(emailAddressBody, EmailAddress.class);

		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + EMAILS + "/" + workflowEmailId + QUEUE;
		return HttpCallService.call(connection, Method.POST, strUri, emailAddress);
	}

	@OutputJsonType(schema = "metadata/automations/automated-email-subscriber-schema.json")
	@DisplayName("Automations: Queue - Get automated email subscriber")
	@Summary("Get information about a specific subscriber in an Automation email queue.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream getAutomatedEmailSubscriber(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Workflow email ID")
		@Summary("The unique id for the Automation workflow email.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowEmailId,

		@DisplayName("Subscriber hash")
		@Summary("The MD5 hash of the lowercase version of the list member's email address.")
		@Expression(ExpressionSupport.SUPPORTED)
		String subscriberHash,

		@ParameterGroup(name = "Fields")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		FieldSpecifier fieldSpecifier

		) {
		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + EMAILS + "/" + workflowEmailId + QUEUE
				+ "/" + subscriberHash;
		MultiMap<String, String> queryParams = new MultiMap<>();

		fieldSpecifier.specify(queryParams);

		return HttpCallService.call(connection, Method.GET, strUri, queryParams);
	}

	/*
	 * Removed Subscribers
	 */

	@OutputJsonType(schema = "metadata/automations/list-subscribers-removed-from-workflow-schema.json")
	@DisplayName("Automations: Removed Subscribers - List subscribers removed from workflow")
	@Summary("Get information about subscribers who were removed from an Automation workflow.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream listSubscribersRemovedFromWorkflow(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@ParameterGroup(name = "Results")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		Results results,

		@ParameterGroup(name = "Fields")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		FieldSpecifier fieldSpecifier

	) {
		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + REMOVED_SUBS;
		MultiMap<String, String> queryParams = new MultiMap<>();

		results.specify(queryParams);
		fieldSpecifier.specify(queryParams);

		return HttpCallService.call(connection, Method.GET, strUri, queryParams);
	}
	@OutputJsonType(schema = "metadata/automations/subscriber-removed-from-workflow-schema.json")
	@DisplayName("Automations: Removed Subscribers - Remove subscriber from workflow")
	@Summary("Remove a subscriber from a specific Automation workflow. You can remove a subscriber at any point in " +
			"an Automation workflow, regardless of how many emails they've been sent from that workflow. Once " +
			"they're removed, they can never be added back to the same workflow.")
	@MediaType(value = "application/json")
	@Throws(BodyCallErrorTypeProvider.class)
	public InputStream removeSubscriberFromWorkflow(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("The unique id for the Automation workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Body")
		@Expression(ExpressionSupport.SUPPORTED)
		@ParameterDsl(allowReferences = false)
		ParameterResolver<EmailAddress> emailAddressBody

	) {
		ExpressionResolver<EmailAddress> resolver = new ExpressionResolver<>();
		EmailAddress emailAddress = resolver.resolve(emailAddressBody, EmailAddress.class);

		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + REMOVED_SUBS;
		return HttpCallService.call(connection, Method.POST, strUri, emailAddress);
	}
	@OutputJsonType(schema = "metadata/automations/subscriber-removed-from-workflow-schema.json")
	@DisplayName("Automations: Removed Subscribers - Get subscriber removed from workflow")
	@Summary("Get information about a specific subscriber who was removed from an Automation workflow.")
	@MediaType(value = "application/json")
	@Throws(CallErrorTypeProvider.class)
	public InputStream getSubscriberRemovedFromWorkflow(
		@Connection MailchimpConnection connection,

		@DisplayName("Workflow ID")
		@Summary("Remove a subscriber from a specific Automation workflow. You can remove a subscriber at any point " +
		"in an Automation workflow, regardless of how many emails they've been sent from that workflow. Once " +
		"they're removed, they can never be added back to the same workflow.")
		@Expression(ExpressionSupport.SUPPORTED)
		String workflowId,

		@DisplayName("Subscriber hash")
		@Summary("The MD5 hash of the lowercase version of the list member's email address.")
		@Expression(ExpressionSupport.SUPPORTED)
		String subscriberHash,

		@ParameterGroup(name = "Fields")
		@Expression(ExpressionSupport.NOT_SUPPORTED)
		@ParameterDsl(allowReferences = false)
		FieldSpecifier fieldSpecifier

	) {
		String strUri = connection.getStrUri() + ENDPOINT + "/" + workflowId + REMOVED_SUBS + "/" + subscriberHash;
		MultiMap<String, String> queryParams = new MultiMap<>();

		fieldSpecifier.specify(queryParams);

		return HttpCallService.call(connection, Method.GET, strUri, queryParams);
	}
}
