/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.IOUtils;
import org.mule.modules.mailchimp.internal.connection.MailchimpConnection;
import org.mule.modules.mailchimp.internal.error.MailchimpErrorType;
import org.mule.modules.mailchimp.internal.error.exception.MailchimpException;
import org.mule.runtime.api.util.MultiMap;
import org.mule.runtime.extension.api.error.MuleErrors;
import org.mule.runtime.http.api.HttpConstants;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.request.HttpRequestBuilder;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static org.mule.modules.mailchimp.internal.util.Parse.*;

public class HttpCallService {

    private static final Logger logger = LoggerFactory.getLogger(HttpCallService.class);

    private HttpCallService() { }

    public static InputStream call(MailchimpConnection connection, HttpConstants.Method method, String strUri,
                                   MultiMap<String, String> queryParams, Object body)  {

        int timeout = connection.getTimeoutAsMilliseconds();

        String strCredentials = connection.getStrCredentials();
        HttpClient client = connection.getHttpClient();

        HttpRequestBuilder builder = HttpRequest.builder()
                .method(method)
                .uri(strUri)
                .addHeader("Authorization", "Basic " + strCredentials);


        if(body != null) {
            try {
                builder.entity(objectToEntity(body));
            }  catch (JsonProcessingException e) {
                logger.error("JSON Parsing error occurred in HttpCallService::call", e);
                throw new MailchimpException("Error parsing body to JSON", MailchimpErrorType.PARSING);
            }
        }

        if(queryParams != null) {
            builder.queryParams(queryParams);
        }

        HttpRequest request = builder.build();
        if (logger.isDebugEnabled()){
            logger.debug(request.toString());
        }

        HttpResponse httpResponse;
        InputStream httpResponseStream;
        byte[] response;

        try {
            httpResponse = client.send(request, timeout, true, null);
            httpResponseStream = httpResponse.getEntity().getContent();

            response = IOUtils.toByteArray(httpResponseStream);
            InputStream copy = new ByteArrayInputStream(response);
            logger.debug(getHttpResponse(copy));

            int statusCode = httpResponse.getStatusCode();
            if (statusCode >= 300) {
                statusCode = statusCode - statusCode%100;

                String reasonPhrase = httpResponse.getReasonPhrase();
                String httpResponseStr;
                httpResponseStr = getHttpResponse(httpResponseStream);

                throw new MailchimpException("Http response status code: " + statusCode + " - " + reasonPhrase
                        + "\n" + httpResponseStr, MailchimpErrorType.HTTP_RESPONSE);
            }

        } catch (IOException e) {
            logger.error("Connection error occurred in HttpCallService::call", e);
            throw new MailchimpException("Error calling HTTP client", MuleErrors.CONNECTIVITY);
        } catch (TimeoutException e) {
            logger.error("Timeout error occurred in HttpCallService::call", e);
            throw new MailchimpException("Timeout calling HTTP client", MuleErrors.CONNECTIVITY);
        }

        return new ByteArrayInputStream(response);
    }

    private static String getHttpResponse(InputStream httpResponseStream) {
        String httpResponseStr;
        try {
            httpResponseStr = inputStreamToString(httpResponseStream);
        } catch (IOException e) {
            logger.error("Error occurred in HttpCallService::getHttpResponse", e);
            httpResponseStr = "There was an error parsing the reason phrase or it was empty.";
        }
        return httpResponseStr;
    }

    public static InputStream call(MailchimpConnection connection,
                                   HttpConstants.Method method, String strUri, MultiMap<String, String> queryParams){

        return call(connection, method, strUri, sanitize(queryParams), null);
    }

    public static InputStream call(MailchimpConnection connection,
                                   HttpConstants.Method method, String strUri, Object body){

        return call(connection, method, strUri, null, body);
    }

    public static InputStream call(MailchimpConnection connection, HttpConstants.Method method, String strUri){

        return call(connection, method, strUri, null, null);
    }

    private static MultiMap<String, String> sanitize(MultiMap<String, String>  queryParams){
        for (Map.Entry<String, String> qp : queryParams.entrySet()){
            queryParams.put(
                    qp.getKey(),
                    sanitize(qp.getValue())
            );
        }
        return queryParams;
    }

    private static String sanitize(String queryParam){
        //Sanitize invalid symbols and spaces (only allowing -, _, . and ,)
        String sanitized = queryParam.replaceAll("([^a-zA-Z0-9_\\-.,])", "");
        //Sanitize values starting with dot, for example: .config
        sanitized = sanitized.replaceAll("(^\\.[a-zA-Z0-9]+)+", "invalid");
        return sanitized;
    }
}
