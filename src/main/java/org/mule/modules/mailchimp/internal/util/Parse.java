/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CloseShieldInputStream;
import org.mule.runtime.http.api.domain.entity.ByteArrayHttpEntity;
import org.mule.runtime.http.api.domain.entity.HttpEntity;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parse {

    private Parse(){ }

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String inputStreamToString(InputStream inputStream) throws IOException {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

            byte[] buffer = new byte[1024];
            int length;

            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }

            return outputStream.toString("UTF-8");
        }
    }

    public static HttpEntity objectToEntity(Object object) throws JsonProcessingException {

        String jsonAsString = objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(object);

        return  new ByteArrayHttpEntity(jsonAsString.getBytes(StandardCharsets.UTF_8));
    }

    public static String cleanMuleFormat(String jsonString) {
        Pattern pattern = Pattern.compile("\\#\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(jsonString);
        matcher.find();
        return matcher.group(1);
    }

}
