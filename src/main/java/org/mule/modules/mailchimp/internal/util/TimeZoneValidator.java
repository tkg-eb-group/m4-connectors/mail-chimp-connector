/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.mailchimp.internal.util;

import org.mule.modules.mailchimp.internal.error.MailchimpErrorType;
import org.mule.modules.mailchimp.internal.error.exception.MailchimpException;

import java.io.Serializable;
import java.util.regex.Pattern;

public class TimeZoneValidator implements Serializable {

    public void validate(String dateStr) {
        Pattern p = Pattern.compile("^[+-][0-5]\\d:[0-5]\\d$");
        if (!p.matcher(dateStr).matches()){
            throw new MailchimpException("Invalid time zone format: " + dateStr, MailchimpErrorType.PARSING);
        }
    }
}
